# PROJET SITE WEB SIMPLON GRENOBLE :

**La maquette réalisée avec** [figma](https://www.figma.com/file/BJ75DWM9CXHgssw70EXsqk/maquette_simplon_web?node-id=0%3A1).


**Technologies utilisées** :

    - HTML5
    - CSS3
    - BOOTSTRAP

**Contenu du site web** :

    - Accueil
    - Simplon International
    - Simplon Grenoble
    - Formation
    - FAQ
    - Partenaires
    - Contact
    - Formulaire d'inscription
    - Mentions légales
    - Sponso (in coming)

**Problèmes connus** :

    - fixation red_quot
    - lien sponso mort
    - toggler icons to change
    - minify style.css ?



## AUTEURS :

*Cedric KADIRA.*
*Jean MIONNET.*
